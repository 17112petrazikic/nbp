using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CassandraDataProvider.DomainModel
{
    public class TuristickiVodic
    {
        public string ime { get; set; }
        public string prezime { get; set; }
        public string naDestinaciji { get; set; }
    }
}
