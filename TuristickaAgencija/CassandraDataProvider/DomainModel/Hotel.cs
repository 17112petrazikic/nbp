﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CassandraDataProvider.DomainModel
{
    public class Hotel
    {
        public string ime { get; set; }
        public string ocena { get; set; }
        public string zvezdice { get; set; }
#nullable enable
        public string? udaljenostOdPlaze { get; set; }
#nullable disable
        public string tv { get; set; }
        public string type { get; set; }
    }
}

