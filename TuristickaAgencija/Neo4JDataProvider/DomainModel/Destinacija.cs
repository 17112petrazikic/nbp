using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neo4JDataLayer.DomainModel
{
    public class Destinacija
    {
        public string id { get; set; }
        public string ime { get; set; }
        public string drzava { get; set; }
        public string opis { get; set; }
#nullable enable
        public string? duzinaPlaze { get; set; }
#nullable disable
        public string imaMore { get; set; }
        public List<string> listaSlika { get; set; }
        public List<Hotel> hoteli { get; set; }
        public Hotel naDestinaciji(Destinacija destinacija, String hotel)
        {
            return null;
        }
    }
}