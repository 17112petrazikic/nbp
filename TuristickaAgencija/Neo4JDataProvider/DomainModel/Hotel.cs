using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neo4JDataLayer.DomainModel
{
    public class Hotel
    {
        public string id { get; set; }
        public string ime { get; set; }
        public string opis { get; set; }
        public string destinacija { get; set; }
        public string komentari { get; set; }
        public string kodovi { get; set; }
        public List<string> listaSlika { get; set; }
        public List<TuristickiVodic> vodici { get; set; }
        public TuristickiVodic radiUHotelu(Hotel hotel, String vodic)
        {
            return null;
        }
    }
}