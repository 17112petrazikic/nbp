using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neo4JDataLayer.DomainModel
{
    public class TuristickiVodic
    {
        public string id { get; set; }
        public string ime { get; set; }
        public string prezime { get; set; }
        public string jezici { get; set; }
        public string uTurizmu { get; set; }
    }
}