using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neo4JDataLayer.DomainModel
{
    public class TuristickaAgencija
    {
        public string id { get; set; }
        public string ime { get; set; }
        public string vlasnik { get; set; }
        public string opis { get; set; }
        public string sediste { get; set; }
        public string radnoVreme { get; set; }
        public string listaFilijala { get; set; }
        public string kontaktTel { get; set; }
        public string cekovi { get; set; }
        public string rate { get; set; }
        public List<string> listaSlika { get; set; }
        public List<Drzava> drzave { get; set; }
        public Drzava uAgenciji(TuristickaAgencija agencija, String drzava)
        {
            return null;
        }
    }
}